﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Leisurefurn_Exercise_Otokiti
{
    public class InvoiceLines
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int LineId { get; set; }

        [StringLength(50)]
        public string InvoiceNumber { get; set; }

        [StringLength(100)]
        public string Description { get; set; }

        public float Quantity { get; set; }

        public float UnitSellingPriceExVAT { get; set; }
    }
}
