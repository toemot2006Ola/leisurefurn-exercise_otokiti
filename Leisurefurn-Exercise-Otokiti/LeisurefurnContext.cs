﻿using System.Data.Entity;

namespace Leisurefurn_Exercise_Otokiti
{
    public class LeisurefurnContext : DbContext
    {
        public DbSet<InvoiceHeader> InvoiceHeaders { get; set; }

        public DbSet<InvoiceLines> InvoiceLines { get; set; }

        public LeisurefurnContext()
            : base("name=DefaultConnection")
        {
        }
    }
}
