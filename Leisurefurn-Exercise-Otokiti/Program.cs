﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;

namespace Leisurefurn_Exercise_Otokiti
{
    class Program
    {
        static void Main(string[] args)
        {
            var context = new LeisurefurnContext();

            var data = LoadCsvList(ConfigurationManager.AppSettings["Path"]);

            foreach (var info in data)
            {
                context.InvoiceHeaders.Add(info.Key);

                context.InvoiceLines.Add(info.Value);
            }

            context.SaveChanges();
            Console.WriteLine("Concluded");

            var totalQuantity = context.InvoiceLines.ToArray();
            foreach (var item in totalQuantity)
            {
                Console.WriteLine($"{item.InvoiceNumber} - {item.Quantity}");
            }

            var test = context.InvoiceLines.Sum(q => q.Quantity * q.UnitSellingPriceExVAT);

            if (test == 21860.71)
            {
                Console.WriteLine("Test Passed");
            }
            else
            {
                Console.WriteLine("Test Failed");
            }
            Console.ReadLine();
        }

        private static List<KeyValuePair<InvoiceHeader, InvoiceLines>> LoadCsvList(string filePath)
        {
            Console.WriteLine("Loading CSV file...");
            var dataType = $"{filePath}";
            var reader = new StreamReader(dataType);
            var dataListTypes = new List<KeyValuePair<InvoiceHeader, InvoiceLines>>();
            var counter = 0;

            while (!reader.EndOfStream)
            {
                counter++;
                var values = reader.ReadLine().Split(',');
                if (values.Length == 0 || counter == 1)
                {
                    continue;
                }

                DateTime.TryParseExact(values[1], "dd/MM/yyyy hh:mm", CultureInfo.InvariantCulture, DateTimeStyles.None, out var temp);

                var lines = new InvoiceLines();
                lines.InvoiceNumber = Convert.ToString(values[0]);
                lines.Description = values[4];
                lines.Quantity = float.Parse(values[5]);
                lines.UnitSellingPriceExVAT = float.Parse(values[6]);

                var headers = new InvoiceHeader();
                headers.InvoiceNumber = values[0];
                headers.InvoiceDate = temp == DateTime.MinValue ? (DateTime?)null : temp;
                headers.InvoiceTotal = float.Parse(values[3]);
                headers.Address = values[2];

                dataListTypes.Add(new KeyValuePair<InvoiceHeader, InvoiceLines>(headers, lines));
            }
            return dataListTypes;
        }
    }

}
